_tz_complete() {

  if [[ "${2}" = "--" ]]; then
    tm --timezones
  elif [[ "${2}" =~ "--.+" ]]; then
    COMPREPLY=( --timezones --help )
  else
    local _tz;
    local _values=()
    for _tz in $(tm --timezones) ; do
      if [[ "${_tz}" =~ "${2}" ]]; then
        _values+=(${_tz})
      fi
    done
  fi
  COMPREPLY=( $( compgen -W "${_values[*]}" ) )
}

complete -F _tz_complete tm