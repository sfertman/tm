#!/bin/sh

npm ci
echo "module.exports = $(./timezones.js | jq);" > timezones-list.js
npx webpack -c webpack.config.js
