# TM

## What?
A really simple (and dumb) nodejs time zone converter for the command line.

## Why?
I got tired of online time zone converters. I mostly live and work in the terminal these days, so it seemed like a good idea.

## Who?
[Luxon](https://www.npmjs.com/package/luxon) 🙏 does *all* the heavy lifting; I just made a cli for a small subset of it.

## How?
`tm` really only answers two *interesting* questions you may have:

a) When it's `TIME` o'clock here, what time is it in timezone `TZ`?

```shell
$ tm 16:20 Africa/Addis_Ababa
# -> Sun, 08 May 2022 23:20:00 +0300
```

b) When in timezone `TZ` it's `TIME` o'clock, what time is it here?

```shell
$ tm Africa/Addis_Ababa 16:20
# -> Sun, 08 May 2022 09:20:00 -0400
```
For more questions refer to:
```shell
$ tm --help
```

## Build
To build a one file script that you can stick anywhere without worrying about node_modules:
```shell
$ ./build.sh
```
Find `tm.js` in `dist` dir

## Use
If you want to use it as a shell command put a symlink in `/usr/local/bin`:
```shell
$ ln -s $PWD/dist/tm.js /usr/local/bin/tm
```

## Autocomplete
Autocompletion is provided by your shell and so, not part of `tm.js`. This is why I wrote `autocomplete.sh` which should provide dumb completion for timezones in your current shell:

```shell
$ source ./autocomplete.sh
```
For a more permanent solution, you can load this file in your `.bashrc` or the equivalent for your shell.

### Known issues which I may or may not fix
- `autocomplete.sh` is case sensitive. I have not found a performant way of making the lowercase cosmetics work for now.
- the timezones list is provided by [@vvo/tzdb](https://www.npmjs.com/package/@vvo/tzdb) 🙏 which is awesome and promises to be super up to date. It does not however, contain everything that luxon CAN handle (which may be a superset of IANA). So some timezones that you know luxon supports may not be autocompleted, for example `Israel`. My only advice is to just try an see if it works. If it doesn't then try to find an official IANA zone that corresponds with the timezone you're interested in and use that instead.
