const webpack = require('webpack');
module.exports = [{
  mode: 'production',
  entry: './tm.js',
  output: {
    path: `${__dirname}/dist`,
    filename: 'tm.js'
  },
  module: {
    rules: [{
      test: /\.(js|jsx)$/i,
      loader: 'babel-loader',
    }]
  },
  target: 'node',
  plugins: [
    new webpack.BannerPlugin({
      banner: '#!/usr/bin/env node',
      raw: true,
      entryOnly: true
    }),
    function() {
      this.hooks.done.tapPromise('Make executable', async () =>
        require('fs').promises.chmod(`${__dirname}/dist/tm.js`, '755')
      );
    }
  ]
}];


