#!/usr/bin/env node

const { DateTime } = require('luxon');

function arrAsStr(arr, indent=0, bullet='-') {
  let prefix = `${new Array(indent).join(' ')}${bullet} `;
  return `${prefix}${arr.join('\n' + prefix)}`
}

function printerr(arr) {
  console.error(`Hmm... 🤔 Could not parse your input; here's why:\n${arrAsStr(arr)}`);
  process.exit(1);
}

function print(obj) {
  console.log(obj);
  process.exit(0);
}

function printdt(dt) {
  !dt.isValid ? printerr(dt.invalid) : print(dt.toRFC2822());
}

function dterr(dt) {
  return `${dt.invalid.reason}: ${dt.invalid.explanation}`;
}

function parseTime(str) {
  return str == 'now' ? DateTime.now() : DateTime.fromISO(str);
}

function parseInterval(str) {
  const ops = { '+': 'plus', '-': 'minus' };
  let [op, hours] = [ str.slice(0,1), str.slice(1) ];
  let err;
  if (!ops[op]) {
    err = `unparsable interval: must start with one the following: ${Object.keys(ops)}`;
  } else if (isNaN(Number(hours))) {
    err = `unparsable interval: "${hours}" is not a valid number of hours`;
  }
  return err ? { err } : { op: ops[op], hours }
}

const ARGS = process.argv.slice(2);

if (ARGS.length == 0) {
  printdt(DateTime.now());
}

if (ARGS.length == 1) { // would be nice to have timezone completion

  if (ARGS[0] == '--timezones') {
    print(require('./timezones-list').join('\n'));
  }

  if (ARGS[0] == '--help' || ARGS[0] == '-h') {
    print(require('./help'));
  }


  // tm now -- print now
  // tm TIME -- print TIME
  // tm +interval -- print now + interval
  // tm -interval -- print now - interval
  // tm TZ -- print now at timezone TZ

  let errors = [];
  let timeFromISO = parseTime(ARGS[0]); // now, TIME
  if (!timeFromISO.isValid) {
    errors.push(dterr(timeFromISO));
  } else {
    printdt(timeFromISO);
  }

  let timeFromInterval;
  let { op, hours, err } = parseInterval(ARGS[0]);
  if (err) {
    errors.push(err)
  } else {
    timeFromInterval = DateTime.now()[op]({hours})
    if (!timeFromInterval.isValid) {
      errors.push(dterr(timeFromInterval));
    } else {
      printdt(timeFromInterval);
    }
  }

  let timeFromTz = DateTime.fromObject({}, { zone: ARGS[0] });
  if (!timeFromTz.isValid) {
    errors.push(dterr(timeFromTz));
  } else {
    printdt(timeFromTz);
  }
  printerr(errors);
}


if (ARGS.length == 2) {
  // tm TIME TZ -- What time TIME is at TZ
  let [time, zone] = ARGS;
  let errors = [];
  let timeWithTz;
  let timeFromISO = parseTime(time);
  if (!timeFromISO.isValid) {
    errors.push(dterr(timeFromISO));
  } else {
    timeWithTz = timeFromISO.setZone(zone);
    if (!timeWithTz.isValid) {
      errors.push(dterr(timeWithTz));
    }
  }

  if (errors.length == 0) {
    printdt(timeWithTz)
  }

  // tm TZ TIME -- What time here is TIME at TZ

  [zone, time] = ARGS;
  timeFromISO = parseTime(time);
  let moreErrors = []
  if (!timeFromISO.isValid) {
    moreErrors.push(dterr(timeFromISO));
  }

  let timeAtTz = DateTime.fromObject(timeFromISO.c, { zone }) // time at given timezone
  let timeAtMyTz = timeAtTz.setZone();
  if (!timeAtMyTz.isValid) {
    moreErrors.push(dterr(timeAtMyTz));
  }

  if (moreErrors.length > 0) {
    printerr([...errors, ...moreErrors]);
  } else {
    printdt(timeAtMyTz);
  }
}

/*

------
0 args
------
tm
  -- print now

-----
1 arg
-----
tm now
  -- print now

tm TIME
  -- print TIME

tm +interval
  -- print now + interval

tm -interval
  -- print now - interval

tm TZ
  -- print now at timezone TZ -- would be nice if I could have shorthand timezones like EDT/PDT/AUDT/UTC/etc

------
2 args
------
tm TIME +interval
  -- print TIME + interval (TIME can be "now")

tm TIME -interval
  -- print TIME - interval (TIME can be "now")

tm TIME TZ -- maybe this is the only one I really
  -- print TIME at timezone TZ

tm TZ TIME
  -- print TZ TIME at my timezone

------
3 args
------
tm TIME +interval TZ
  -- print TIME + interval at timezone TZ

tm TIME -interval TZ
  -- print TIME - interval at timezone TZ

*/