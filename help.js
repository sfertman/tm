module.exports = `
Usage:  tm [OPTION] | [[ARG1] [ARG2]]

A simple timezone converter

Arguments:
  now          print current time
  TIME         attempt to parse TIME as ISO format and print
  TZ           print current time at timezone TZ
  +INTERVAL    print time in INTERVAL hours
  -INTERVAL    print time INTERVAL hours ago

  TIME TZ      print time TIME at TZ
  TZ TIME      print TZ time in current timezone

Options:
  --timezones  print all timesone names
  -h | --help  print help
`;
